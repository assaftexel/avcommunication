// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "EngagementCloudAVCommunicationKit",
    platforms: [.iOS(.v12), .tvOS(.v14)],
    products: [
        .library(
            name: "EngagementCloudAVCommunicationKit",
            targets: ["EngagementCloudAVCommunicationKit",
                      "AgoraAIDenoiseExtension","AgoraCIExtension","AgoraCore",
                      "AgoraDav1dExtension","AgoraFDExtension","Agorafdkaac",
                      "Agoraffmpeg","AgoraJNDExtension","AgoraRtcKit",
                      "AgoraSoundTouch","AgoraSuperResolutionExtension","AgoraVideoSegmentationExtension"]),
    ],
    dependencies: [
        .package(name: "EngagementCloudLoggerKit",
                 url: "https://github.com/getndazn/ios-engagement-cloud-logger-sdk",
                 .branch("develop")),
        .package(name: "EngagementCloudNetworkingKit",
                 url: "https://github.com/getndazn/ios-engagement-cloud-networking-sdk",
                 .branch("develop")),
        .package(name: "EngagementCloudUserSessionKit",
                 url: "https://github.com/getndazn/ios-engagement-cloud-user-session-sdk",
                 .branch("develop")),
    ],
    targets: [
        .target(
            name: "EngagementCloudAVCommunicationKit",
            dependencies: [
                .product(name: "EngagementCloudLoggerKit",
                         package: "EngagementCloudLoggerKit"),
                .product(name: "EngagementCloudUserSessionKit",
                         package: "EngagementCloudUserSessionKit"),
                .product(name: "EngagementCloudNetworkingKit",
                         package: "EngagementCloudNetworkingKit"),
                "AgoraAIDenoiseExtension","AgoraCIExtension","AgoraCore",
                "AgoraDav1dExtension","AgoraFDExtension","Agorafdkaac",
                "Agoraffmpeg","AgoraJNDExtension","AgoraRtcKit",
                "AgoraSoundTouch","AgoraSuperResolutionExtension","AgoraVideoSegmentationExtension",
            ]),
        .binaryTarget(
            name: "AgoraAIDenoiseExtension",
            path: "Sources/Agora/AgoraAIDenoiseExtension.xcframework"
        ),
        .binaryTarget(
            name: "AgoraCIExtension",
            path: "Sources/Agora/AgoraCIExtension.xcframework"
        ),
        .binaryTarget(
            name: "AgoraCore",
            path: "Sources/Agora/AgoraCore.xcframework"
        ),
        .binaryTarget(
            name: "AgoraDav1dExtension",
            path: "Sources/Agora/AgoraDav1dExtension.xcframework"
        ),
        .binaryTarget(
            name: "AgoraFDExtension",
            path: "Sources/Agora/AgoraFDExtension.xcframework"
        ),
        .binaryTarget(
            name: "Agorafdkaac",
            path: "Sources/Agora/Agorafdkaac.xcframework"
        ),
        .binaryTarget(
            name: "Agoraffmpeg",
            path: "Sources/Agora/Agoraffmpeg.xcframework"
        ),
        .binaryTarget(
            name: "AgoraJNDExtension",
            path: "Sources/Agora/AgoraJNDExtension.xcframework"
        ),
        .binaryTarget(
            name: "AgoraRtcKit",
            path: "Sources/Agora/AgoraRtcKit.xcframework"
        ),
        .binaryTarget(
            name: "AgoraSoundTouch",
            path: "Sources/Agora/AgoraSoundTouch.xcframework"
        ),
        .binaryTarget(
            name: "AgoraSuperResolutionExtension",
            path: "Sources/Agora/AgoraSuperResolutionExtension.xcframework"
        ),
        .binaryTarget(
            name: "AgoraVideoSegmentationExtension",
            path: "Sources/Agora/AgoraVideoSegmentationExtension.xcframework"
        ),
    ]
)
