//
//  AVCommunicationEncryptionMode.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Elad on 12/07/2021.
//

import Foundation

public enum AVCommunicationEncryptionMode: String, CaseIterable {
    case aes_128_xts
    case aes_128_ecb
    case aes_256_xts
    case aes_128_gcm
    case aes_256_gcm
    case aes_128_gcm2
    case aes_256_gcm2
    
    var isSaltRequired: Bool {
        switch self {
        case .aes_128_gcm2, .aes_256_gcm2: return true
        default: return false
        }
    }
}
