//
//  AVCommunicationEncryptionState.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 03/10/2021.
//

import Foundation

enum AVCommunicationEncryptionState {
    case enable(AVCommunicationEncryptionConfig)
    case disable
}
