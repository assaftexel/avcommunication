//
//  AVCommunicationFrameRate.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Elad on 01/07/2021.
//

import Foundation

public enum AVCommunicationFrameRate: String, CaseIterable {
    case vd_15fps
    case vd_24fps
    case vd_30fps
}
