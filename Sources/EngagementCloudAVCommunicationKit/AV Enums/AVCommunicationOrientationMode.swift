//
//  AVCommunicationOrientationMode.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Elad on 01/07/2021.
//

import Foundation

public enum AVCommunicationOrientationMode: String, CaseIterable {
    case adaptive
    case landscape
    case portrait
}
