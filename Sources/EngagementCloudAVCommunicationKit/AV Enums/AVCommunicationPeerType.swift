//
//  AVCommunicationPeerView.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 03/08/2021.
//

import Foundation

public enum AVCommunicationPeerType {
    case local
    case remote(peerId: String)
}
