//
//  AVCommunicationResolution.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Elad on 01/07/2021.
//

import Foundation

public enum AVCommunicationResolution: String, CaseIterable {
    case vd_160X120
    case vd_320X180
    case vd_320X240
    case vd_640X360
}
