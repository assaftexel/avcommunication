//
//  AVCommunicationRole.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Elad on 01/07/2021.
//

import Foundation

public enum AVCommunicationRole: String, CaseIterable {
    case moderator
    case coHost
    case participant
}
