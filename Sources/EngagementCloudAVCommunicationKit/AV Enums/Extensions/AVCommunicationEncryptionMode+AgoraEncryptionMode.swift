//
//  AVCommunicationEncryptionMode+AgoraEncryptionMode.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 26/07/2021.
//

import Foundation
import AgoraRtcKit

extension AVCommunicationEncryptionMode {
    var agoraEncryptionMode: AgoraEncryptionMode {
        switch self {
        case .aes_128_xts:
            return .AES128XTS
        case .aes_128_ecb:
            return .AES128ECB
        case .aes_256_xts:
            return .AES256XTS
        case .aes_128_gcm:
            return .AES128GCM
        case .aes_256_gcm:
            return .AES256GCM
        case .aes_128_gcm2:
            return .AES128GCM2
        case .aes_256_gcm2:
            return .AES256GCM2
        }
    }
}
