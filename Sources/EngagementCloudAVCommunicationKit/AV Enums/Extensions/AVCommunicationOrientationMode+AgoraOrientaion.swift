//
//  AVCommunicationOrientationMode+AgoraOrientaion.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 26/07/2021.
//

import Foundation
import AgoraRtcKit

extension AVCommunicationOrientationMode {
    var agoraOrientaion: AgoraVideoOutputOrientationMode {
        switch self {
        case .adaptive:
            return .adaptative
        case .landscape:
            return .fixedLandscape
        case .portrait:
            return .fixedPortrait
        }
    }
}
