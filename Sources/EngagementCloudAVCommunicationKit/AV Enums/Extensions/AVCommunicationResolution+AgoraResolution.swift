//
//  AVCommunicationResolution+AgoraResolution.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 26/07/2021.
//

import Foundation
import AgoraRtcKit

extension AVCommunicationResolution {
    var agoraResolution: CGSize {
        switch self {
        case .vd_160X120:
            return CGSize(width: 160, height: 120)
        case .vd_320X180:
            return CGSize(width: 320, height: 180)
        case .vd_320X240:
            return CGSize(width: 320, height: 240)
        case .vd_640X360:
            return CGSize(width: 640, height: 360)
        }
    }
}

