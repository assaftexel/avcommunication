//
//  AVCommunicationConnectionError.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 02/08/2021.
//

import Foundation


public enum AVCommunicationConnectionError: LocalizedError {
    case tokenExpired
    case invalidToken
    
    case invalidAppId
    case invalidArgument
    case networkDown
    case joinChannelRejected
    case leaveChannelRejected
    case invalidChannelId
    case notInChannel
    case decryptionFailed
    case startCallFailed
    
    case videoCaptureFailure
    case audioCaptureFailure
}

public extension AVCommunicationConnectionError {
    var errorDescription: String? {
        switch self {
        case .tokenExpired:
            return "token has expired".localized
        case .invalidToken:
            return "token is invalid".localized
        case .invalidAppId:
            return "app id is invalid".localized
        case .invalidArgument:
            return "invalid argument".localized
        case .networkDown:
            return "network is down".localized
        case .joinChannelRejected:
            return "join channel rejected".localized
        case .leaveChannelRejected:
            return "leave channel rejected".localized
        case .invalidChannelId:
            return "invalid channel id".localized
        case .notInChannel:
            return "peer not in channel".localized
        case .decryptionFailed:
            return "decryption failed".localized
        case .startCallFailed:
            return "Fails to start the call after enabling the media engine".localized
        case .videoCaptureFailure:
            return "Failure in capturing video".localized
        case .audioCaptureFailure:
            return "Failure in capturing audio".localized
        }
    }
}
