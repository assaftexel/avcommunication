//
//  AVCommunicationError.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Elad on 01/07/2021.
//

import Foundation
import NetworkingKit

public enum AVCommunicationError: LocalizedError {
    case sdkNotInitialized
    case sdkAlreadyInitialized
    case coreNotInitialized
    
    case notLoggedIn
    
    case noCameraPermission
    case noMicrophonePermission
    
    case notJoinedRoom
    case alreadyJoinedRoom
    
    case unableToParseTokenConfigJson
    case unableToParseTokenJson
    case invalidUrl
    case errorFetchingAVCommunicationToken(NetworkingError)
    
    case connectionError(AVCommunicationConnectionError)
    
    case encryptionKeyIsEmpty
    case saltIsNil
    case saltIsNot32Bytes
    case saltIsZero
    
    case noSuchPeer
   case unknownError
}

public extension AVCommunicationError {
     var errorDescription: String? {
        switch self {
            case .sdkNotInitialized:
                return "sdk not initialized yet".localized
            case .sdkAlreadyInitialized:
                return "sdk is already initialized".localized
            case .noCameraPermission:
                return "no camera permission provided".localized
            case .noMicrophonePermission:
                return "no microphone permission provided".localized
            case .notJoinedRoom:
                return "not joined room".localized
            case .alreadyJoinedRoom:
                return "already joined room".localized
            case .unableToParseTokenJson:
                return "unable to parse token json".localized
            case .unableToParseTokenConfigJson:
                return "unable to parse token config json".localized
            case .invalidUrl:
                return "invalid url".localized
            case .errorFetchingAVCommunicationToken(_):
                return "error fetching AVCommunication token".localized
            case .encryptionKeyIsEmpty:
                return "encryption key is empty".localized
            case .saltIsNil:
                return "salt is nil".localized
            case .saltIsNot32Bytes:
                return "salt is not 32 bytes".localized
            case .saltIsZero:
                return "salt is zero".localized
            case .noSuchPeer:
                return "no such peer".localized
            case .coreNotInitialized:
                return "core not initialized".localized
            case .notLoggedIn:
                return "user not logged in".localized
            case .unknownError:
                return "unknown error".localized
            case .connectionError(let error):
                return "connection error: \(error)".localized
        }
    }
}
