//
//  AVCommunicationConnectionError+AgoraConnectionError.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 02/08/2021.
//

import Foundation
import AgoraRtcKit

extension AVCommunicationConnectionError {
    static func avCommunicationConnectionError(of agoraErrorCode: AgoraErrorCode) -> AVCommunicationConnectionError? {
        switch agoraErrorCode {
        case .invalidAppId: return .invalidAppId
        case .invalidArgument: return .invalidArgument
        case .netDown: return .networkDown
        case .joinChannelRejected: return .joinChannelRejected
        case .leaveChannelRejected: return .leaveChannelRejected
        case .invalidChannelId: return .invalidChannelId
        case .notInChannel: return .notInChannel
        case .decryptionFailed: return .decryptionFailed
        case .startCall: return .startCallFailed
        default: return nil
        }
    }
    
    static func avCommunicationConnectionError(of agoraConnectionChangedReason: AgoraConnectionChangedReason) -> AVCommunicationConnectionError? {
        switch  agoraConnectionChangedReason {
        case .tokenExpired: return .tokenExpired
        case .invalidToken: return .invalidToken
        case .invalidAppId: return .invalidAppId
        default: return nil
        }
    }
    
    static func avCommunicationConnectionError(of agoraLocalVideoStreamError: AgoraLocalVideoStreamError) -> AVCommunicationConnectionError? {
        switch agoraLocalVideoStreamError {
        case .captureFailure: return .videoCaptureFailure
        default: return nil
        }
    }
    
    static func avCommunicationConnectionError(of agoraAudioLocalError: AgoraAudioLocalError) -> AVCommunicationConnectionError? {
        switch agoraAudioLocalError {
        case .recordFailure: return .audioCaptureFailure
        default: return nil
        }
    }
}
