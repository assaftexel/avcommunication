//
//  AVCommunicationFeature.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri on 30/06/2021.
//

import Foundation
import AgoraRtcKit
import EngagementCloudUserSessionKit

public typealias AVCommunicationResult = Result<Void, AVCommunicationError>

// MARK: - AVCommunicationFeature
public class AVCommunicationFeature {
    // MARK: - Public Static Properties
    /// Version of the SDK.
    public static var version: String { "\(AgoraRtcEngineKit.getSdkVersion()).\(Consts.VERSION)"  }
    
    // MARK: - Public Properties
    /// The current AV Communication state.
    public var state: AVCommunicationState? { createState() }
    
    /// The capturing configuration of the streaming.
    public var avCommunicationCaptureConfig: AVCommunicationCaptureConfig = .default {
        didSet {
            guard isPrepared else { return }
            set(avCommunicationCaptureConfig: avCommunicationCaptureConfig)
        }
    }
    
    // MARK: - Private Properties
    // For future use when the roles will have functionality
    private var role: AVCommunicationRole = .coHost { didSet { roleChanged(to: role) } }
    
    
    private var localVideoCanvas: AgoraRtcVideoCanvas?
    private var uidToVideoCanvas: [UInt: AgoraRtcVideoCanvas] = [:]
    private lazy var avCommunicationAgoraDelegateWrapper = AVCommunicationAgoraDelegateWrapper(avCommunicationFeature: self)
    private let avCommunicationManager = AVCommunicationManager()
    private var localPeer: AVCommunicationPeer?
    private let roomId: String
    
    // MARK: - FilePrivate Properties
    fileprivate var isPrepared = false
    fileprivate var isJoinedRoom: Bool = false
    fileprivate var rtcEngine: AgoraRtcEngineKit?
    fileprivate var uidToPeerIdCache: [UInt: String] = [:] // cache that only cleared when leaving the room
    fileprivate var uidToPeer: [UInt: AVCommunicationPeer] = [:]
    fileprivate var peerIdToUid: [String: UInt] = [:]
    fileprivate var prepareCompletionHandler: ((AVCommunicationResult) -> Void)?
    
    // MARK: - Delegate
    /// The delegate instance for raising AV Communication events.
    public weak var delegate: AVCommunicationDelegate?
    
    // MARK: - Initializers
    /// Initialize with room ID.
    /// - Parameter roomId: The room ID to initialize the av communication feature for.
    public init(roomId: String) {
        log("AVCommunicationFeature initialized in constructor with room Id: \(roomId)")
        self.roomId = roomId
    }
    
    deinit { destroy() }
    
    // MARK: - Public Methods
    /// Prepare the AV Communication.
    /// - Parameters:
    ///   - avComEncryptionConfig: Configuration of the AV Communication encryption.
    ///   - completionHandler: A callback for indicating when the preparing proccess finished.
    public func prepare(with avCommunicationEncryptionConfig: AVCommunicationEncryptionConfig?, completionHandler: @escaping (AVCommunicationResult) -> Void) {
        log("In preparing")
        
        guard !isPrepared else {
            log("in preparing, error: sdkAlreadyInitialized")
            completionHandler(.failure(.sdkAlreadyInitialized))
            return
        }
        
        let result = UserSession.shared.validate(roomId: roomId, andAdd: self)
        switch result {
            case .failure(let userSessionError):
                switch userSessionError {
                    case .notInRoom:
                        completionHandler(.failure(.notJoinedRoom))
                    case .notLoggedIn:
                        completionHandler(.failure(.notLoggedIn))
                    case .coreIsNotInitiazlied:
                        completionHandler(.failure(.coreNotInitialized))
                }
                return
            case .success(_):
                guard let userId = UserSession.shared.sessionInfo?.user.userId,
                      let deviceId = UserSession.shared.sessionInfo?.user.deviceId
                else { completionHandler(.failure(.notLoggedIn)); return }
                
                self.localPeer = AVCommunicationPeer(peerId: "\(userId)_\(deviceId)")
        }
        
        avCommunicationManager.subscribeTo(roomId: roomId) { result in
            switch result {
                case .failure(let error):
                    completionHandler(AVCommunicationResult.fail(.errorFetchingAVCommunicationToken(error)))
                    return
                case .success(let agoraToken):
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else {
                            completionHandler(.failure(.unknownError))
                            return
                        }
                        
                        self.rtcEngine = AgoraRtcEngineKit.sharedEngine(withAppId: agoraToken.agoraAppId,
                                                                        delegate: self.avCommunicationAgoraDelegateWrapper)
                        self.rtcEngine?.enableDualStreamMode(true)
                        self.rtcEngine?.setRemoteDefaultVideoStreamType(.high)
                        self.rtcEngine?.setParameters("{\"che.video.lowBitRateStreamParameter\":{\"width\":160,\"height\":120,\"frameRate\":5,\"bitRate\":45}}")
                        self.rtcEngine?.setChannelProfile(.liveBroadcasting)
                        self.rtcEngine?.setClientRole(.broadcaster)
                        
                        self.configureAudio()
                        self.configureVideo(with: self.avCommunicationCaptureConfig)
                        
                        self.prepareCompletionHandler = completionHandler
                        
                        self.joinRoom(with: self.roomId, agoraToken: agoraToken, avCommunicationEncryptionConfig: avCommunicationEncryptionConfig) {
                            [weak self] joinResult in
                            switch joinResult {
                                case .success(_):
                                    self?.isPrepared = true
                                    self?.prepareCompletionHandler = nil
                                    completionHandler(.success())
                                case .failure(let error):
                                    completionHandler(.failure(error))
                            }
                        }
                        
                    }
            }
        }
    }
    
    /// Destory all resources.
    public func destroy() {
        log("in destroying AVCommunicationFeature")
        if rtcEngine != nil {
            if isJoinedRoom { rtcEngine?.leaveChannel(nil) }
            rtcEngine = nil
            AgoraRtcEngineKit.destroy()
        }
        reset()
    }
    
    /// Start video and audio stream to and from room
    public func startStreams() throws {
        log("In start streams")
        
        guard isPrepared else {
            log("in startStreams, error: noInitiazlized")
            return
        }
        
        guard PermissionChecker.checkMicrophonePermission() else {
            log("checkMicrophonePermission, no microphone permission")
            throw AVCommunicationError.noMicrophonePermission
        }
        
        guard PermissionChecker.checkCameraPermission() else {
            log("checkCameraPermission, no camera permission")
            throw AVCommunicationError.noCameraPermission
        }
        
        internalStartStreams()
    }
    
    /// Stop video and audio streams
    public func stopStreams() {
        log("In stop streams")
        
        guard isPrepared else {
            log("in stopStreams, error: noInitiazlized")
            return
        }
        
        internalStopStreams()
    }
    
    /// Enable or disable camera,
    /// - Parameter enabled: Represents enable or disable the camera.
    public func camera(enabled: Bool) throws {
        log("In camera enabled: \(enabled)")
        
        guard isPrepared else {
            log("in camera, error: noInitiazlized")
            return
        }
        
        log("Before - camera muted: \(String(describing: localPeer?.isVideoMuted))")
        
        if enabled {
            guard PermissionChecker.checkCameraPermission() else {
                log("in camera, no camera permission")
                throw AVCommunicationError.noCameraPermission
            }
            startVideoStream()
        }
        else { stopVideoStream() }
        
        log("After - camera muted: \(String(describing: localPeer?.isVideoMuted))")
    }
    
    /// Enable or disable microphone,
    /// - Parameter enabled: Represents enable or disable the microphone.
    public func microphone(enabled: Bool) throws {
        log("In microphone enabled: \(enabled)")
        
        guard isPrepared else {
            log("in microphone, error: noInitiazlized")
            return
        }
        
        log("Before - microphone muted: \(String(describing: localPeer?.isAudioMuted))")
        
        if enabled {
            guard PermissionChecker.checkMicrophonePermission() else {
                log("checkMicrophonePermission, no microphone permission")
                throw AVCommunicationError.noMicrophonePermission
            }
            startAudioStream()
        }
        else { stopAudioStream() }
        
        log("After - microphone muted: \(String(describing: localPeer?.isAudioMuted))")
    }
    
    /// Switches between front and rear cameras.
    public func switchCamera() throws {
        log("In switch camera")
        
        guard isPrepared else {
            log("in switchCamera, error: noInitiazlized")
            return
        }
        
        guard PermissionChecker.checkCameraPermission() else {
            log("in switchCamera, no camera permission")
            throw AVCommunicationError.noCameraPermission
        }
        
        rtcEngine?.switchCamera()
    }
    
    /// Set view of local or remote peer.
    /// - Parameters:
    ///   - view: The view that will present peers video.
    ///   - avCommunicationPeerType: local or remote peer.
    public func set(view: UIView?, for avCommunicationPeerType: AVCommunicationPeerType) throws {
        log("In set view for \(avCommunicationPeerType)")
        
        guard isPrepared else {
            log("in set view, error: noInitiazlized")
            return
        }
        
        switch avCommunicationPeerType {
            case .local:
                log("In set view for local peer")
                setLocalVideo(view: view)
            case .remote(let peerId):
                log("In set view for peer id: \(peerId)")
                guard let uid = peerIdToUid[peerId] else {
                    log("in set view, noSuchPeer: \(peerId)")
                    throw AVCommunicationError.noSuchPeer
                    
                }
                setRemoteVideo(view: view, for: uid)
        }
    }
    
    /// Mute or unmute all remote video streams
    /// - Parameter mute: bool value that indicate if we should mute or unmute all remote video streams
    public func muteAllRemoteVideoStreams(mute: Bool) {
        log("In mute all remote video streams")
        
        guard isPrepared else {
            log("in mute all remote video streams, error: noInitiazlized")
            return
        }
        
        rtcEngine?.muteAllRemoteVideoStreams(mute)
    }
    
    /// Mute or unmute all remote audio streams
    /// - Parameter mute: bool value that indicate if we should mute or unmute all remote audio streams
    public func muteAllRemoteAudioStreams(mute: Bool) {
        log("In mute all remote audio streams")
        
        guard isPrepared else {
            log("in mute all remote audio streams, error: noInitiazlized")
            return
        }
        
        rtcEngine?.muteAllRemoteAudioStreams(mute)
    }
    
    // MARK: - Private Methods
    private func joinRoom(with roomId: String, agoraToken: AgoraToken, avCommunicationEncryptionConfig: AVCommunicationEncryptionConfig?, completionHandler: @escaping (AVCommunicationResult) -> Void) {
        log("In joining room: \(roomId)")
        
        guard rtcEngine != nil, let localPeer = localPeer else { completionHandler(AVCommunicationResult.fail(.sdkNotInitialized)); return }
        guard !isJoinedRoom else { completionHandler(AVCommunicationResult.fail(.alreadyJoinedRoom)); return }
        
        if let avCommunicationEncryptionConfig = avCommunicationEncryptionConfig { set(avCommunicationEncryptionState: .enable(avCommunicationEncryptionConfig)) }
        else { set(avCommunicationEncryptionState: .disable) }
        
        rtcEngine?.joinChannel(byUserAccount: localPeer.peerId, token: agoraToken.agoraToken, channelId: roomId) { [weak self] _,_,_ in
            log("Joined room: \(roomId)")
            self?.isJoinedRoom = true
            completionHandler(AVCommunicationResult.success())
        }
    }
    
    private func roleChanged(to role: AVCommunicationRole) {
        log("in role changed")
        
        guard isPrepared else { log("Role was not changed, Not Initialized"); return }
        guard role == .participant else { log("Role was not changed, role is not participant"); return }
        
        internalStopStreams()
    }
    
    private func createState() -> AVCommunicationState? {
        log("In create state")
        
        guard isJoinedRoom, let localPeer = localPeer else { return nil }
        
        let avCommunicationState = AVCommunicationState(remotePeers: Set<AVCommunicationPeer>(uidToPeer.map{ $0.value }), localPeer: localPeer, joinedRoom: isJoinedRoom)
        
        log("state: \(avCommunicationState)")
        
        return avCommunicationState
    }
    
    private func configureAudio() {
        log("In configure audio")
        
        rtcEngine?.enableAudio()
        
        rtcEngine?.enableAudioVolumeIndication(500, smooth: 1, report_vad: true)
        rtcEngine?.setDefaultAudioRouteToSpeakerphone(true)
        rtcEngine?.setAudioProfile(.default, scenario: .default)
        
        stopAudioStream()
    }
    
    private func configureVideo(with avCommunicationCaptureConfig: AVCommunicationCaptureConfig) {
        log("In configure video")
        
        rtcEngine?.enableVideo()
        
        set(avCommunicationCaptureConfig: avCommunicationCaptureConfig)
        stopVideoStream()
    }
    
    private func set(avCommunicationCaptureConfig: AVCommunicationCaptureConfig) {
        log("In set avCommunicationCaptureConfig - resolution: \(avCommunicationCaptureConfig.avCommunicationResolution), frame rate: \(avCommunicationCaptureConfig.avCommunicationFrameRate), bitrate: adaptive, orientation: \(avCommunicationCaptureConfig.avCommunicationOrientationMode)")
        
        rtcEngine?.setVideoEncoderConfiguration(AgoraVideoEncoderConfiguration(size: avCommunicationCaptureConfig.avCommunicationResolution.agoraResolution, frameRate: avCommunicationCaptureConfig.avCommunicationFrameRate.agoraFrameRate, bitrate: 0, orientationMode: avCommunicationCaptureConfig.avCommunicationOrientationMode.agoraOrientaion))
    }
    
    private func internalStartStreams() {
        log("In internal start streams")
        
        startAudioStream()
        startVideoStream()
    }
    
    private func internalStopStreams() {
        log("in internal stop streams")
        
        stopAudioStream()
        stopVideoStream()
    }
    
    private func startVideoStream() {
        log("In start video stream")
        
        guard role != .participant else { log("Video stream was not started, role is participant"); return }
        
        enableCamera(true)
    }
    
    private func stopVideoStream() {
        log("In stop video stream")
        enableCamera(false)
    }
    
    private func enableCamera(_ enabled: Bool) {
        log("In enable camera - enabled: \(enabled)")
        
        rtcEngine?.enableLocalVideo(enabled)
        rtcEngine?.muteLocalVideoStream(!enabled)
        
        localPeer?.isVideoMuted = !enabled
    }
    
    private func startAudioStream() {
        log("In start audio stream")
        
        guard role != .participant else { log("Audio stream was not started, role is participant"); return }
        enableMicrophone(true)
    }
    
    private func stopAudioStream() {
        log("In stop audio stream")
        
        enableMicrophone(false)
        delegate?.avCommunicationFeature(self, localPeerSpeaking: false)
    }
    
    private func enableMicrophone(_ enabled: Bool) {
        log("In enable camera - enabled: \(enabled)")
        
        rtcEngine?.enableLocalAudio(enabled)
        rtcEngine?.muteLocalAudioStream(!enabled)
        
        localPeer?.isAudioMuted = !enabled
    }
    
    private func set(avCommunicationEncryptionState: AVCommunicationEncryptionState) {
        let agoraEncryptionConfig = AgoraEncryptionConfig()
        let enableEncryption: Bool
        
        switch avCommunicationEncryptionState {
            case .enable(let avCommunicationEncryptionConfig):
                log("In set avCommunicationEncryptionState - enabling encryption: encryption mode: \(avCommunicationEncryptionConfig.avCommunicationEncryptionMode), encryption key: \(avCommunicationEncryptionConfig.avCommunicationEncryptionKey), encryption salt size: \(String(describing: avCommunicationEncryptionConfig.avCommunicationEncryptionSalt?.count))")
                
                agoraEncryptionConfig.encryptionKdfSalt = avCommunicationEncryptionConfig.avCommunicationEncryptionSalt
                agoraEncryptionConfig.encryptionKey = avCommunicationEncryptionConfig.avCommunicationEncryptionKey
                agoraEncryptionConfig.encryptionMode = avCommunicationEncryptionConfig.avCommunicationEncryptionMode.agoraEncryptionMode
                enableEncryption = true
            case .disable:
                log("In set avCommunicationEncryptionState - disabling encryption")
                enableEncryption = false
        }
        
        rtcEngine?.enableEncryption(enableEncryption, encryptionConfig: agoraEncryptionConfig)
    }
    
    private func leaveChannel(completion: (() -> Void)? = nil) {
        log("In leave channel")
        rtcEngine?.leaveChannel { [weak self] _ in
            self?.reset()
            completion?()
        }
    }
    
    private func setLocalVideo(view: UIView?) {
        log("In set local peer video - remove view: \(view == nil)")
        
        guard let view = view else {
            log("Remove view for local peer")
            localVideoCanvas = nil
            rtcEngine?.setupLocalVideo(nil)
            return
        }
        
        if let localVideoCanvas = localVideoCanvas {
            log("Update video view for local peer")
            localVideoCanvas.view = view
        }
        else {
            log("Create video canvas for local peer")
            localVideoCanvas = createVideoCanvas(view: view, for: 0)
        }
        
        rtcEngine?.setupLocalVideo(localVideoCanvas!)
    }
    
    private func setRemoteVideo(view: UIView?, for uid: UInt) {
        log("In set remote peer video - for uid: \(uid), remove view: \(view == nil)")
        
        if let view = view {
            if let videoCanvas = uidToVideoCanvas[uid] {
                log("Update video canvas found for uid: \(uid), peer id: \(String(describing: uidToPeer[uid]?.peerId))")
                videoCanvas.view = view
                rtcEngine?.setupRemoteVideo(videoCanvas)
            } else {
                log("Creating video canvas for uid: \(uid), peer id: \(String(describing: uidToPeer[uid]?.peerId))")
                let videoCanvas = createVideoCanvas(view: view, for: uid)
                uidToVideoCanvas[uid] = videoCanvas
                rtcEngine?.setupRemoteVideo(videoCanvas)
            }
        } else if let videoCanvas = uidToVideoCanvas[uid] {
            log("Removing video canvas found for uid: \(uid), peer id: \(String(describing: uidToPeer[uid]?.peerId))")
            videoCanvas.view = nil
            rtcEngine?.setupRemoteVideo(videoCanvas)
            uidToVideoCanvas.removeValue(forKey: uid)
        } else {
            log("Not Found video canvas for removal for uid: \(uid), peer id: \(String(describing: uidToPeer[uid]?.peerId))")
        }
    }
    
    private func createVideoCanvas(view: UIView, for uid: UInt) -> AgoraRtcVideoCanvas {
        log("In create video canvas")
        
        let videoCanvas = AgoraRtcVideoCanvas()
        
        videoCanvas.view = view
        videoCanvas.uid = uid
        videoCanvas.renderMode = .hidden
        
        return videoCanvas
    }
    
    private func reset() {
        log("In reset")
        uidToPeer.removeAll()
        peerIdToUid.removeAll()
        uidToPeerIdCache.removeAll()
        
        uidToVideoCanvas.forEach { $0.value.view = nil }
        uidToVideoCanvas.removeAll()
        
        localVideoCanvas?.view = nil
        localVideoCanvas = nil
        
        role = .coHost
        
        isJoinedRoom = false
        
        stopAudioStream()
        stopVideoStream()
    }
    
    // MARK: - FilePrivate Methods
    fileprivate func addPeer(with peerId: String, and uid: UInt) {
        log("In add peer - peerId: \(peerId), uid: \(uid)")
        uidToPeer[uid] = AVCommunicationPeer(peerId: peerId)
        peerIdToUid[peerId] = uid
    }
    
    fileprivate func removePeer(with peerId: String, and uid: UInt) {
        log("In remove peer - peerId: \(peerId), uid: \(uid)")
        uidToPeer.removeValue(forKey: uid)
        peerIdToUid.removeValue(forKey: peerId)
    }
}

// MARK: - UserSessionDelegate
extension AVCommunicationFeature: UserSessionDelegate {
    public func userSession(leftRoom userSession: UserSessionable) { destroy() }
}

// MARK: - AgoraRtcEngineDelegate
fileprivate class AVCommunicationAgoraDelegateWrapper: NSObject, AgoraRtcEngineDelegate {
    
    private weak var avCommunicationFeature: AVCommunicationFeature!
    
    // MARK: - initializers
    fileprivate init(avCommunicationFeature: AVCommunicationFeature) {
        self.avCommunicationFeature = avCommunicationFeature
    }
    
    // MARK: - Connection Delegates
    func rtcEngine(_ engine: AgoraRtcEngineKit, connectionChangedTo state: AgoraConnectionStateType, reason: AgoraConnectionChangedReason) {
        if state == .failed, let error = AVCommunicationConnectionError.avCommunicationConnectionError(of: reason) {
            log("In inner AVCommunicationFeature delegate connectionChangedTo - with error: \(error)")
            if let prepareCompletionHandler = avCommunicationFeature?.prepareCompletionHandler {
                avCommunicationFeature?.prepareCompletionHandler = nil
                AgoraRtcEngineKit.destroy()
                avCommunicationFeature?.rtcEngine = nil
                avCommunicationFeature?.isJoinedRoom = false
                avCommunicationFeature?.isPrepared = false
                prepareCompletionHandler(.fail(.connectionError(error)))
            } else {
                avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, errorOccured: error)
            }
        } else {
            log("In inner AVCommunicationFeature delegate connectionChangedTo - with unkown error: \(reason)")
        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        if let error = AVCommunicationConnectionError.avCommunicationConnectionError(of: errorCode) {
            log("In inner AVCommunicationFeature delegate didOccurError - with error: \(error)")
            avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, errorOccured: error)
        } else {
            log("In inner AVCommunicationFeature delegate didOccurError - with unkown error: \(errorCode)")
        }
    }
    
    // MARK: - Join Room Delegates
    func rtcEngine(_ engine: AgoraRtcEngineKit, didUpdatedUserInfo userInfo: AgoraUserInfo, withUid uid: UInt) {
        log("In inner AVCommunicationFeature delegate didUpdatedUserInfo - user account: \(String(describing: userInfo.userAccount))")
        
        guard let peerId = userInfo.userAccount else { return }
        
        avCommunicationFeature.uidToPeerIdCache[uid] = peerId
        avCommunicationFeature.addPeer(with: peerId, and: uid)
        avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, peerJoinedRoom: peerId)
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        log("In inner AVCommunicationFeature delegate didJoinedOfUid - with uid: \(uid), peerId: \(String(describing: avCommunicationFeature.uidToPeerIdCache[uid])), uidToPeer[uid] == nil: \(avCommunicationFeature.uidToPeer[uid] == nil)")
        
        guard let peerId = avCommunicationFeature.uidToPeerIdCache[uid], avCommunicationFeature.uidToPeer[uid] == nil else { return }
        
        log("In inner AVCommunicationFeature delegate didJoinedOfUid - with uid: \(uid), with peerId: \(peerId)")
        
        avCommunicationFeature.addPeer(with: peerId, and: uid)
        
        avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, peerJoinedRoom: peerId)
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didRejoinChannel channel: String, withUid uid: UInt, elapsed: Int) {
        log("In inner AVCommunicationFeature delegate didRejoinChannel - channelId: \(channel), uid: \(uid)")
        
        avCommunicationFeature.isJoinedRoom = true
        
        avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, localPeerRejoinedTo: channel)
    }
    
    // MARK: - Leave Room Delegates
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        log("In inner AVCommunicationFeature delegate didOfflineOfUid - peerId: \(String(describing: avCommunicationFeature.uidToPeer[uid])), reason != AgoraUserOfflineReason.becomeAudience: \(reason != AgoraUserOfflineReason.becomeAudience)")
        
        guard let peer = avCommunicationFeature.uidToPeer[uid], reason != AgoraUserOfflineReason.becomeAudience else { return }
        
        avCommunicationFeature.removePeer(with: peer.peerId, and: uid)
        avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, peerLeftRoom: peer.peerId)
    }
    
    // MARK: - Video Delegates
    func rtcEngine(_ engine: AgoraRtcEngineKit, localVideoStateChange state: AgoraLocalVideoStreamState, error: AgoraLocalVideoStreamError) {
        if let error = AVCommunicationConnectionError.avCommunicationConnectionError(of: error) {
            log("In inner AVCommunicationFeature delegate localVideoStateChange - with error: \(error)")
            avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, errorOccured: error)
        } else {
            log("In inner AVCommunicationFeature delegate localVideoStateChange - with unkown error: \(error)")
        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didVideoMuted muted: Bool, byUid uid: UInt) {
        log("In inner AVCommunicationFeature delegate didVideoMuted - peerId: \(String(describing: avCommunicationFeature.uidToPeer[uid])), muted: \(muted)")
        
        guard avCommunicationFeature.uidToPeer[uid] != nil else { return }
        
        avCommunicationFeature.uidToPeer[uid]!.isVideoMuted = muted
        
        avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, remotePeer: avCommunicationFeature.uidToPeer[uid]!.peerId, isVideoMuted: avCommunicationFeature.uidToPeer[uid]!.isVideoMuted)
    }
    
    // MARK: - Audio Delegates
    func rtcEngine(_ engine: AgoraRtcEngineKit, localAudioStateChange state: AgoraAudioLocalState, error: AgoraAudioLocalError) {
        if let error = AVCommunicationConnectionError.avCommunicationConnectionError(of: error) {
            log("In inner AVCommunicationFeature delegate localAudioStateChange - with error: \(error)")
            avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, errorOccured: error)
        } else {
            log("In inner AVCommunicationFeature delegate localAudioStateChange - with unkown error: \(error)")
        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didAudioMuted muted: Bool, byUid uid: UInt) {
        log("In inner AVCommunicationFeature delegate didAudioMuted - peerId: \(String(describing: avCommunicationFeature.uidToPeer[uid])), muted: \(muted)")
        
        guard avCommunicationFeature.uidToPeer[uid] != nil else { return }
        
        avCommunicationFeature.uidToPeer[uid]!.isAudioMuted = muted
        avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, remotePeer: avCommunicationFeature.uidToPeer[uid]!.peerId, isAudioMuted: avCommunicationFeature.uidToPeer[uid]!.isAudioMuted)
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, reportAudioVolumeIndicationOfSpeakers speakers: [AgoraRtcAudioVolumeInfo], totalVolume: Int) {
        if speakers.isEmpty {
            log("In inner AVCommunicationFeature delegate speakers - no speakers")
            avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, peersAreSpeaking: [])
        }
        
        else if speakers.count == 1, speakers[0].uid == 0 {
            log("In inner AVCommunicationFeature delegate speakers - local peer is speaking: \(speakers[0].volume >= Consts.MIN_SPEAKER_VOLUME)")
            avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, localPeerSpeaking: speakers[0].volume >= Consts.MIN_SPEAKER_VOLUME)
        }
        
        else {
            let tempSpeakers = speakers
                .filter { avCommunicationFeature.uidToPeer[$0.uid] != nil && $0.volume >= Consts.MIN_SPEAKER_VOLUME }
                .map { avCommunicationFeature.uidToPeer[$0.uid]!.peerId }
            
            if tempSpeakers.isEmpty {
                log("In inner AVCommunicationFeature delegate speakers - no speakers")
                avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, peersAreSpeaking: [])
            } else {
                log("In inner AVCommunicationFeature delegate speakers - remote peers speaking: \(tempSpeakers)")
                avCommunicationFeature.delegate?.avCommunicationFeature(avCommunicationFeature, peersAreSpeaking: tempSpeakers)
            }
        }
    }
}
