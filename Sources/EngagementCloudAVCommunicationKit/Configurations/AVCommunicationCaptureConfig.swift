//
//  AVCommunicationCaptureConfig.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Elad on 01/07/2021.
//

import Foundation

public struct AVCommunicationCaptureConfig {
    public let avCommunicationResolution: AVCommunicationResolution
    public let avCommunicationFrameRate: AVCommunicationFrameRate
    public let avCommunicationOrientationMode: AVCommunicationOrientationMode
    
    public init(avCommunicationResolution: AVCommunicationResolution, avCommunicationFrameRate: AVCommunicationFrameRate, avCommunicationOrientationMode: AVCommunicationOrientationMode) {
        self.avCommunicationResolution = avCommunicationResolution
        self.avCommunicationFrameRate = avCommunicationFrameRate
        self.avCommunicationOrientationMode = avCommunicationOrientationMode
    }
}
