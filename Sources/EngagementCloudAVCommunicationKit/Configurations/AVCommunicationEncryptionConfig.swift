//
//  AVCommunicationEncryptionConfig.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 26/07/2021.
//

import Foundation

public struct AVCommunicationEncryptionConfig {
    public let avCommunicationEncryptionKey: String
    public let avCommunicationEncryptionMode: AVCommunicationEncryptionMode
    public let avCommunicationEncryptionSalt: Data?
    
    public init(avCommunicationEncryptionKey: String, avCommunicationEncryptionMode: AVCommunicationEncryptionMode, avCommunicationEncryptionSalt: Data?) throws {
        if avCommunicationEncryptionMode.isSaltRequired {
            if let salt = avCommunicationEncryptionSalt {
                if salt.count != 32 { throw AVCommunicationError.saltIsNot32Bytes }
                if salt.isZero { throw AVCommunicationError.saltIsZero }
                
                self.avCommunicationEncryptionSalt = salt
            } else { throw AVCommunicationError.saltIsNil }
        } else { self.avCommunicationEncryptionSalt = nil }
        
        if avCommunicationEncryptionKey.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty { throw AVCommunicationError.encryptionKeyIsEmpty }
        
        self.avCommunicationEncryptionKey = avCommunicationEncryptionKey
        self.avCommunicationEncryptionMode = avCommunicationEncryptionMode
    }
}


fileprivate extension Data {
    var isZero: Bool { !self.contains { $0 != 0 } }
}
