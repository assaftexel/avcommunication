//
//  AVCommunicationCaptureConfig+Default.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 03/08/2021.
//

import Foundation

public extension AVCommunicationCaptureConfig {
    static let `default` = AVCommunicationCaptureConfig(avCommunicationResolution: .vd_320X240, avCommunicationFrameRate: .vd_15fps, avCommunicationOrientationMode: .adaptive)
}
