//
//  Result+Failure.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 31/08/2021.
//

import Foundation

extension AVCommunicationResult {
    static func fail(_ error: Failure, line: Int = #line, file: String = #file) -> AVCommunicationResult {
        log("\(error)", file: file, line: line)
        return Result.failure(error)
    }
}
