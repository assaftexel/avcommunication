//
//  Result+Success.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 29/07/2021.
//

import Foundation

extension AVCommunicationResult {
    static func success() -> AVCommunicationResult { Result.success(()) }
}
