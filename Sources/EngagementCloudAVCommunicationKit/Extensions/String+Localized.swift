//
//  String+Localized.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 30/08/2021.
//

import Foundation

internal extension String {
    var localized: String { NSLocalizedString(self, comment: "") }
}
