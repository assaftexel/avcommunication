//
//  String+RemoveFromEdges.swift
//  
//
//  Created by Assaf Tayouri - DAZN X on 29/12/2021.
//

import Foundation

extension String {
    func removeFromEdges(character: Character) -> String {
        var str = self
        
        if str.first == character { str = String(str.dropFirst()) }
        if str.last == character { str = String(str.dropLast()) }
        
        return str
    }
}
