//
//  AVCommunicationManager.swift
//  
//
//  Created by Assaf Tayouri - DAZN X on 29/12/2021.
//

import Foundation
import EngagementCloudNetworkingKit
import NetworkingKit

// MARK: - URLPostfixes
fileprivate struct URLPostfixes {
    static let subscribeToRoom = "/v2/avcommunication/subscriptions/{roomId}"
}

// MARK: - AVCommunicationManager
class AVCommunicationManager {
    func subscribeTo(roomId: String, completionHandler: @escaping (Result<AgoraToken, NetworkingError>) -> Void) {
        EngagementCloudHttpClient.shared
            .createTask(with: HttpRequest(method: .POST,
                                               postfixUrl: URLPostfixes.subscribeToRoom
                                                .replacingOccurrences(of: "{roomId}",
                                                                      with: roomId
                                                                        .trimmingCharacters(in: .whitespacesAndNewlines)
                                                                        .removeFromEdges(character: "/"))))
            .start(parseTo: AgoraToken.self) { result in
                DispatchQueue.global().async {
                    switch result {
                        case .failure(let error):
                            completionHandler(.failure(error))
                        case .success(let agoraToken):
                            completionHandler(.success(agoraToken))
                    }
                }
            }
    }
}
