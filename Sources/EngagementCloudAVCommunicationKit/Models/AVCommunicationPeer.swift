//
//  AVCommunicationPeer.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 26/07/2021.
//

import Foundation

public struct AVCommunicationPeer {
    public let peerId: String
    public internal(set) var isVideoMuted: Bool
    public internal(set) var isAudioMuted: Bool
    
    init(peerId: String, isVideoMuted: Bool = true, isAudioMuted: Bool = true) {
        self.peerId = peerId
        self.isVideoMuted = isVideoMuted
        self.isAudioMuted = isAudioMuted
    }
}

extension AVCommunicationPeer: Hashable {
    public func hash(into hasher: inout Hasher) { hasher.combine(peerId) }
    public static func == (lhs: Self, rhs: Self) -> Bool { lhs.peerId == rhs.peerId }
}
