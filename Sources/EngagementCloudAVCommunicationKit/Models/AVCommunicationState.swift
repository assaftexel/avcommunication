//
//  AVCommunicationState.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 26/07/2021.
//

import Foundation

public struct AVCommunicationState {
    public let remotePeers: Set<AVCommunicationPeer>
    public let localPeer: AVCommunicationPeer
    public let joinedRoom: Bool
    
    init(remotePeers: Set<AVCommunicationPeer>, localPeer: AVCommunicationPeer, joinedRoom: Bool) {
        self.remotePeers = remotePeers
        self.localPeer = localPeer
        self.joinedRoom = joinedRoom
    }
}
