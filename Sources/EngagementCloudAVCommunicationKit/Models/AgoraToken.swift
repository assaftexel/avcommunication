//
//  AgoraToken.swift
//  
//
//  Created by Assaf Tayouri - DAZN X on 10/01/2022.
//

import Foundation


struct AgoraToken: Decodable {
    let agoraToken: String
    let agoraAppId: String
}
