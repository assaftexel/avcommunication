//
//  AVCommunicationDelegate.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Elad on 01/07/2021.
//

import Foundation

// MARK: - protocol AVCommunicationDelegate
public protocol AVCommunicationDelegate: AnyObject {
    /// Local peer rejoined room.
    /// - Parameters:
    ///   - avCommunicationFeature: AV Communication feature that calls the delegate.
    ///   - roomId: unique room id string.
    func avCommunicationFeature(localPeerRejoinedRoom avCommunicationFeature: AVCommunicationFeature)
    
    /// peer joined room.
    /// - Parameters:
    ///   - avCommunicationFeature: AV Communication feature that calls the delegate.
    ///   - peerId: string that represents the peer ID.
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, peerJoinedRoom peerId: String)
    
    /// peer left room.
    /// - Parameters:
    ///   - avCommunicationFeature: AV Communication feature that calls the delegate.
    ///   - peerId: string that represents the peer ID.
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, peerLeftRoom peerId: String)
    
    /// Local peer start or stop speaking.
    /// - Parameters:
    ///   - avCommunicationFeature: AV Communication feature that calls the delegate.
    ///   - isSpeaking: Indicator if the local peer is speaking.
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, localPeerSpeaking isSpeaking: Bool)
    
    /// which of the remote peers are speaking.
    /// - Parameters:
    ///   - avCommunicationFeature: AV Communication feature that calls the delegate.
    ///   -  peerIds: array of strings. each element is a peer Id.
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, peersAreSpeaking peerIds: [String])
    
    /// Remote peers audio is muted or not.
    /// - Parameters:
    ///   - avCommunicationFeature: AV Communication feature that calls the delegate.
    ///   - peerId: string that represents the peer ID.
    ///   - isAudioMuted: Indicator if the audio of remote peer is muted.
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, remotePeer peerId: String, isAudioMuted: Bool)
    
    /// Remote peers video is muted or not.
    /// - Parameters:
    ///   - avCommunicationFeature: AV Communication feature that calls the delegate.
    ///   - peerId: string that represents the peer ID.
    ///   - isVideoMuted: Indicator if the video of remote peer is muted.
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, remotePeer peerId: String, isVideoMuted: Bool)
    
    /// Error handler.
    /// - Parameters:
    ///   - avCommunicationFeature: AV Communication feature that calls the delegate.
    ///   - error: AV Connection error type.
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, errorOccured error: AVCommunicationConnectionError)
}

// MARK: - extension AVCommunicationDelegate
/// Empty AVCommunicationDelegate implementation
public extension AVCommunicationDelegate {
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, localPeerRejoinedTo roomId: String){}
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, peerJoinedRoom peerId: String){}
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, peerLeftRoom peerId: String){}
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, localPeerSpeaking isSpeaking: Bool){}
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, peersAreSpeaking peerIds: [String]){}
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, remotePeer peerId: String, isAudioMuted: Bool){}
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, remotePeer peerId: String, isVideoMuted: Bool){}
    func avCommunicationFeature(_ avCommunicationFeature: AVCommunicationFeature, errorOccured error: AVCommunicationConnectionError){}
}
