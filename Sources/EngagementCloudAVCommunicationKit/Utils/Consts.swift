//
//  Consts.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 26/07/2021.
//

import Foundation
import EngagementCloudLoggerKit
import LoggerKit

fileprivate var logger: Logger { EngagementCloudLogger.shared }

func log(_ items: Any..., priority: Priority = .info, file: String = #file, line: Int = #line) {
    logger.log("EngagementCloudAVCommunication", items, priority: priority, line: line, file: file)
}

struct Consts {
    static let MIN_SPEAKER_VOLUME = 20
    static let TAG = "EngagementCloudAVCommunication"
    static let VERSION = "1.2.6"
}
