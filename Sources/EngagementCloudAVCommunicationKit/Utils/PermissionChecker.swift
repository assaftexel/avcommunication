//
//  PermissionChecker.swift
//  EngagementCloudAVCommunicationKit
//
//  Created by Assaf Tayouri - DAZN X on 26/07/2021.
//

import Foundation
import AVFoundation

class PermissionChecker {
    static func checkCameraPermission() -> Bool { checkPermission(for: .video) }
    
    static func checkMicrophonePermission() -> Bool { checkPermission(for: .audio) }
    
    private static func checkPermission(for mediaType: AVMediaType) -> Bool { AVCaptureDevice.authorizationStatus(for: mediaType) ==  .authorized }
}
